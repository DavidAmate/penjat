import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Penjat {
	static int i=0;
	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
			ArrayList<String> ParaulesFitxer=MostraContingut("paraules.txt");//pasem el fitxer per ficar a un arraylist les paraules que conte
			String EndevinaParaula=ParaulaRandom(ParaulesFitxer);//Agafem la paraula random entre las que tenim al fitxer
			int s=0;
			String Pantalla="";
			while(s<EndevinaParaula.length())//Fem el string que veura el jugador amb les mateixes * que lletres te la paraula
			{
				Pantalla=Pantalla +"*";
				s++;
			}
			Play(Pantalla,EndevinaParaula);//Bucle principal del joc
			
			
		
	}
	static ArrayList<String> MostraContingut(String archivo) throws FileNotFoundException, IOException {
	      String cadena;
	      FileReader f = new FileReader(archivo);      
	      BufferedReader b = new BufferedReader(f);
	      ArrayList <String> paraules=new ArrayList <String>();
	      while((cadena = b.readLine())!=null) {
	          paraules.add(cadena);
	      }
	      b.close();
	      return paraules;
	      
	}
	static String ParaulaRandom(ArrayList<String> paraulesFitxer)
	{
		Random TriaParaula=new Random();//Ho utilitzarem per agafar un numero random
		int ParaulaEscollida=TriaParaula.nextInt(paraulesFitxer.size());//Agafem una posicio random i la guardem a un int
		String EndevinaParaula =paraulesFitxer.get(ParaulaEscollida);// Fiquem la paraula de aquesta posicio a un string
		return EndevinaParaula;//Retornem la paraula a endevinar
	}
	static void Play(String Pantalla,String EndevinaParaula)
	{
		boolean f=false;
		int maxIntents=10;
		boolean find;
		Scanner inputDevice = new Scanner( System.in );
		StringBuilder PantallaB = new StringBuilder(Pantalla);//String que es va actualitzant, conte el string que hem creat abans amb les *
		System.out.println("Hola benvingut al joc del penjat de David Amate");
		while (!f)
		{
			find=false;
			if(10>maxIntents)//Aixo es per a que la primera vegada no dibuixi res
			{
			   PintarMatriu(maxIntents+1);//S'encarrega d'anar mostrant el penjat segons el nombre d'intents restants
			}
			if (maxIntents<=0)//Si gastes tots els intents perds i et mostra la paraula
			{
				System.out.println("Has perdut el joc!!!");
				System.out.println("La paraula era " +EndevinaParaula);
				f=true;
				break;
			}		
		int z=0;
		System.out.println("La paraula a endevinar es la "+ PantallaB);//Mostro la paraula a endevinar amb * i que s'anira actualitzant
		System.out.println("Et queden "+ maxIntents +" intents");
		System.out.println("Introdueix una lletra per veure si aquesta esta dins la paraula");
		char lletra =inputDevice.next().charAt(0);//Nomes agafo la primera posicio per si escriuen diverses lletres
		
		while (z <EndevinaParaula.length())
		{
			
			if (EndevinaParaula.charAt(z)==lletra)//Busco si aquesta lletra la conte la paraula a endevinar
			{
				if(PantallaB.charAt(z)!=lletra)//Si la conte i encara no esta pintada la pinto
				{
					System.out.println("Has encertat una lletra!");
					PantallaB.setCharAt(z, lletra);
					z++;
					find=true;
				}
				else if(PantallaB.charAt(z)==lletra)//Si ja esta pintada li dic que ja forma part de la paraula
				{
					System.out.println("La lletra "+lletra+" ja forma part de la paraula");
					z++;
					find=true;
				}		
			}
			else z++;
		}
		if(PantallaB.toString().equals(EndevinaParaula))//Si la paraula a endevinar equival a la que ha anat construint el jugador guanyes el joc
		{
			f=true;
			System.out.println("Has guanyat el joc!!!");
			System.out.println("La paraula era " +EndevinaParaula);
		}
		
		if(!find)//Si la lletra no s'ha trobat restem 1 als intents restants
		{
			maxIntents--;
			System.out.println("La lletra "+lletra+" no forma part de la paraula");
		}
		}
	}
	static void PintarMatriu(int maxIntents)
	{
		String[][] forca= new String[][]{{" "," "," "," "},{" "," ", " "," "},{" "," "," "," "},{" ", " "," "," "},{" "," "," "," "}};
		switch(maxIntents)//segons el nombre d'intents restants cambiem el contingut de l'array
		{
			case 1:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/","O","/"},{"|","/",".","/"},{"|","_____"," "," "}};;break;
			case 2:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/","O","/"},{"|","/"," ","/"},{"|","_____"," "," "}};;break;
			case 3:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/","O","/"},{"|","/"," "," "},{"|","_____"," "," "}};;break;
			case 4:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/","O","/"},{"|"," "," "," "},{"|","_____"," "," "}};break;
			case 5:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/","O"," "},{"|"," "," "," "},{"|","_____"," "," "}};;break;
			case 6:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|","/"," "," "},{"|"," "," "," "},{"|","_____"," "," "}};break;
			case 7:forca=new String[][]{{"____", " "," "," "},{"|"," ","o"," "},{"|"," "," "," "},{"|"," "," "," "},{"|","_____"," "," "}};break;
			case 8:forca=new String[][]{{"____", " "," "," "},{"|"," "," "," "},{"|"," "," "," "},{"|"," "," "," "},{"|","_____"," "," "}};break;
			case 9:forca=new String[][]{{" "," "," "," "},{"|"," "," "," "},{"|"," "," "," "},{"|"," "," "," "},{"|","_____"," "," "}};break;
			case 10:forca=new String[][]{{" ", " "," "," "},{" "," "," "," "},{" "," "," "," "},{" "," "," "," "},{" ","_____"," "," "}};;break;
			default:break;
		}
		  for (int x=0; x < forca.length; x++) //Bucle per recorrer l'array
		  {
		        for (int y=0; y < forca[x].length; y++) 
		        {
		          System.out.print (forca[x][y]);
		          
		        }
		        System.out.println("\n");//Serveix per fer un salt de linia
		      }
		  }
}
